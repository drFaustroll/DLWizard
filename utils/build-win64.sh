#!/usr/bin/env bash

mkdir -p build-win64
pushd build-win64
FFLAGS="-O3 -static" cmake ../ -DCMAKE_TOOLCHAIN_FILE=../cmake/win64helper.cmake 

make -j10
make package
