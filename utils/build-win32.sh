#!/usr/bin/env bash

rm -rf build-win32
mkdir -p build-win32
pushd build-win32
FFLAGS="-O3 -static" cmake ../ -DCMAKE_TOOLCHAIN_FILE=../cmake/win32helper.cmake  
make -j10 VERBOSE=1
make package
