#!/usr/bin/env bash 

rm -rf build-lin64
mkdir -p build-lin64
pushd build-lin64
cmake ../ -DBUILD_SHARED_LIBS=On -DWITH_INTERNAL_QCUSTOMPLOT=Off
make -j4
popd
