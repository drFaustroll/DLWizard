# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#.rst:
# FindQCustomPlot
# --------
#
# Find the native QCustomPlot includes and libraries.
# QCustomPlot is a Qt C++ widget for plotting and data visualization. It has no further dependencies and is well documented. This
# plotting library focuses on making good looking, publication quality 2D plots, graphs and charts, as well as offering high
# performance for realtime visualization applications.
# It is free software under the GNU General Public
# License.
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# If QCustomPlot is found, this module defines the following
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module will set the following variables in your project
#
#  QCP_FOUND          - True if QCustomPlot found on the local system
#  QCP_INCLUDE_DIRS   - Location of QCustomPlot header files.
#  QCP_LIBRARIES      - The QCustomPlot libraries.
find_package(PkgConfig QUIET)

pkg_check_modules(PC_QCP QUIET qcustomplot-qt5)

# Look for the header file.
find_path(QCP_INCLUDE_DIR NAMES qcustomplot.h HINTS ${PC_QCP_INCLUDE_DIRS})

# Look for the library.
find_library(QCP_LIBRARY NAMES qcustomplot-qt5 libqcustomplot-qt5 HINTS ${PC_QCP_LIBRARY_DIRS})
message(INFO " -- ${QCP_LIBRARY}")
# handle the QUIETLY and REQUIRED arguments and set QCP_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(QCP
                                  REQUIRED_VARS QCP_LIBRARY QCP_INCLUDE_DIR)

# Copy the results to the output variables.
if(QCP_FOUND)
  set(QCP_LIBRARIES ${QCP_LIBRARY})
  set(QCP_INCLUDE_DIRS ${QCP_INCLUDE_DIR})
endif()

mark_as_advanced(QCP_INCLUDE_DIR QCP_LIBRARY)
