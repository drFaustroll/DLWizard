//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include <QtWidgets>
#include <QDateTime>
#include <QtPrintSupport>
#include <iostream>

#include "DLWizard.h"
#include "utils.h"

DLWizard::DLWizard(QMainWindow *parent) : QMainWindow(parent) {
  widget = new QWidget(this);
  setCentralWidget(widget);
  info = new QTextEdit(this);
  info->setText(tr("<i>Welcome to DLWizard!!!</i>"));
  info->append(tr("Started: ") + QDateTime().currentDateTime().toString());
  info->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  info->setAlignment(Qt::AlignLeft);
  info->setReadOnly(1);

  QVBoxLayout *layout = new QVBoxLayout;
  layout->setMargin(5);
  layout->addWidget(info);
  widget->setLayout(layout);

  createActions();
  createMenus();

  QString message = tr("Welcome to DLWizard!!!");
  statusBar()->showMessage(message);

  setWindowTitle(tr("DLWizard"));
  QRect screen = QDesktopWidget().screenGeometry(0);
  setMinimumSize(screen.width() / 4.0, screen.height() / 4.0);
  resize(screen.width() * 0.75, screen.height() * 0.75);
}
void DLWizard::loadStatis() {
  info->append(tr("Executed: ") + QDateTime().currentDateTime().toString());
  QFileDialog::Options options = QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  QString statName = QFileDialog::getOpenFileName(
     this, "DL_POLY Statistics file", "", "STATIS files (STATIS);;STATIS (*)",
     &selectedFilter, options);
  if (statName.isEmpty()) {
    info->append("Aborted <b>Load Statistics file</b>");
  } else {
    info->append("<b>Load Statistics file</b> " + statName);
    dlStatis = new DLStatis(statName, info);
    info->append("<b>Length of time series</b> " +
                 QString("%1").arg(dlStatis->getLength()));
  }
}

void DLWizard::loadFrame() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  QFileDialog::Options options = QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  QString frameName = QFileDialog::getOpenFileName(
     this, "DL_POLY CONFIG/REVCON", "",
     "CONFIG files (CONFIG);;REVCON (REVCON);;CONFIG/REVCON (*)",
     &selectedFilter, options);
  if (frameName.isEmpty()) {
    info->append("Aborted <b>Load Frame</b>");
  } else {
    info->append("<b>Load Frame</b> " + frameName);
    dlFrame = new DLFrame(frameName, info);
    info->append(QString("<b>Loaded %1 atoms</b> ").arg(dlFrame->getNAtoms()));
    info->append(
       QString("<b>Loaded %1 species</b> ").arg(dlFrame->getNSpecies()));
  }
}

void DLWizard::loadTrajectory() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  QFileDialog::Options options = QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  QString trajectoryName = QFileDialog::getOpenFileName(
     this, "DL_POLY HISTORY", "", "HISTORY files (HISTORY);;HISTORY (*)",
     &selectedFilter, options);
  if (trajectoryName.isEmpty()) {
    info->append("Aborted <b>Load Trajectory</b>");
  } else {
    info->append("<b>Load Trajectory</b> " + trajectoryName);
  }
}

void DLWizard::save() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  // we can use the native file dialog but creates more troubles than usefulness
  QFileDialog::Options options = QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  QString logName = QFileDialog::getSaveFileName(
     this, "DLWizard Log", "", "Log files (*.log);;Text files (*.txt)",
     &selectedFilter, options);
  if (logName.isEmpty()) {
    info->append("Aborted <b>Save Log!!!</b>");
  } else {
    info->append("Invoked <b>Save Log:</b> to file" + logName);
    QFile log(logName);
    log.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream olog(&log);
    olog << info->toPlainText() << endl;
    log.close();
  }
}

void DLWizard::print() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>File|Print Log</b>"));
  QPrinter printer;
  QPrintDialog dialog(&printer, this);
  dialog.setWindowTitle("Print Log");
  if (dialog.exec() == QDialog::Accepted) {
    info->print(dialog.printer());
  }
}

void DLWizard::copy() {
  info->copy();
  info->append(tr("Invoked <b>Edit|Copy</b>"));
}

void DLWizard::clear() {
  info->clear();
  info->setText(tr("<b>User cleared the view</b>"));
  info->append("<i>Welcome to DLWizard!!!</i>");
  info->append("Started: " + QDateTime().currentDateTime().toString());
}

void DLWizard::select() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Edit|Select All</b>"));
  info->selectAll();
  info->setFocus();
}

void DLWizard::statis() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append("Invoked <b>Analysis|STATIS</b>");
  if (dlStatis != nullptr) {
    DLStatisPlotter *statW = new DLStatisPlotter(dlStatis, info, this);
  } else {
    info->append("No STATIS loaded!!!");
  }
}

void DLWizard::rdfShow() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|RDF Show</b>"));
}

void DLWizard::rdfCalc() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|RDF Calc</b>"));
}

void DLWizard::sk() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|S(k)</b>"));
}

void DLWizard::zDensity() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|Z Density</b>"));
}

void DLWizard::msd() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|MSD</b>"));
}

void DLWizard::vaf() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|VAF</b>"));
}

void DLWizard::faf() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Analysis|FAF</b>"));
}

void DLWizard::about() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Help|About</b>"));
  QMessageBox::about(this, tr("About Menu"),
                     tr("The <b>DLWizard</b> is a very very very simple "
                        "DL_POLY data shower and post run analysis. "
                        "In a perfect world this will not exist and be merged "
                        "in Aten a much superior tool"));
}

void DLWizard::aboutQt() {
  info->append("Executed: " + QDateTime().currentDateTime().toString());
  info->append(tr("Invoked <b>Help|About Qt</b>"));
}

void DLWizard::createActions() {
  loadStatisAct = new QAction(tr("L&oad Statistics"), this);
  loadStatisAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_H));
  loadStatisAct->setStatusTip(tr("Load STATIS"));
  connect(loadStatisAct, SIGNAL(triggered()), this, SLOT(loadStatis()));

  loadFrameAct = new QAction(tr("L&oad Frame"), this);
  loadFrameAct->setShortcuts(QKeySequence::Open);
  loadFrameAct->setStatusTip(tr("Load CONFIG/REVCON"));
  connect(loadFrameAct, SIGNAL(triggered()), this, SLOT(loadFrame()));

  loadTrajAct = new QAction(tr("Load &Trajectory"), this);
  loadTrajAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_T));
  loadTrajAct->setStatusTip(tr("Load HISTORY"));
  connect(loadTrajAct, SIGNAL(triggered()), this, SLOT(loadTrajectory()));

  saveAct = new QAction(tr("&Save Log"), this);
  saveAct->setShortcuts(QKeySequence::Save);
  saveAct->setStatusTip(tr("Save the log to disk"));
  connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

  printAct = new QAction(tr("&Print Log"), this);
  printAct->setShortcuts(QKeySequence::Print);
  printAct->setStatusTip(tr("Print the log"));
  connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcuts(QKeySequence::Quit);
  exitAct->setStatusTip(tr("Exit the application"));
  connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

  copyAct = new QAction(tr("&Copy"), this);
  copyAct->setShortcuts(QKeySequence::Copy);
  copyAct->setStatusTip(
     tr("Copy the current selection's contents to the clipboard"));
  connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));

  clearAct = new QAction(tr("C&lear"), this);
  clearAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_L));
  clearAct->setStatusTip(tr("Clear the info view"));
  connect(clearAct, SIGNAL(triggered()), this, SLOT(clear()));

  selectAct = new QAction(tr("Select &All"), this);
  selectAct->setShortcuts(QKeySequence::SelectAll);
  selectAct->setStatusTip(tr("Select all the content"));
  connect(selectAct, SIGNAL(triggered()), this, SLOT(select()));

  statisAct = new QAction(tr("&STATIS"), this);
  statisAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_S));
  statisAct->setStatusTip(tr("Inspect STATIS"));
  connect(statisAct, SIGNAL(triggered()), this, SLOT(statis()));

  rdfShowAct = new QAction(tr("&RDF Show"), this);
  rdfShowAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_R));
  rdfShowAct->setStatusTip(tr("Show RDF"));
  connect(rdfShowAct, SIGNAL(triggered()), this, SLOT(rdfShow()));

  rdfCalcAct = new QAction(tr("RDF &Calc"), this);
  rdfCalcAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_C));
  rdfCalcAct->setStatusTip(tr("Calculate RDF"));
  connect(rdfCalcAct, SIGNAL(triggered()), this, SLOT(rdfCalc()));

  skAct = new QAction(tr("S(&k)"), this);
  skAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_K));
  skAct->setStatusTip(tr("S(k)"));
  connect(skAct, SIGNAL(triggered()), this, SLOT(sk()));

  zDensityAct = new QAction(tr("&z Density"), this);
  zDensityAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_Z));
  zDensityAct->setStatusTip(tr("z Density"));
  connect(zDensityAct, SIGNAL(triggered()), this, SLOT(zDensity()));

  msdAct = new QAction(tr("&MSD"), this);
  msdAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_M));
  msdAct->setStatusTip(tr("MSD"));
  connect(msdAct, SIGNAL(triggered()), this, SLOT(msd()));

  vafAct = new QAction(tr("&VAF"), this);
  vafAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_V));
  vafAct->setStatusTip(tr("VAF"));
  connect(vafAct, SIGNAL(triggered()), this, SLOT(vaf()));

  fafAct = new QAction(tr("FAF"), this);
  fafAct->setShortcut(QKeySequence(Qt::ALT + Qt::Key_O));
  fafAct->setStatusTip(tr("FAF"));
  connect(fafAct, SIGNAL(triggered()), this, SLOT(faf()));

  aboutAct = new QAction(tr("&About"), this);
  aboutAct->setStatusTip(tr("Show the application's About box"));
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  aboutQtAct = new QAction(tr("About &Qt"), this);
  aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
  connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
  connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));
}

void DLWizard::createMenus() {
  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(loadStatisAct);
  fileMenu->addAction(loadFrameAct);
  fileMenu->addAction(loadTrajAct);
  fileMenu->addSeparator();
  fileMenu->addAction(saveAct);
  fileMenu->addAction(printAct);
  fileMenu->addSeparator();
  fileMenu->addAction(exitAct);

  editMenu = menuBar()->addMenu(tr("&Edit"));
  editMenu->addAction(copyAct);
  editMenu->addAction(clearAct);
  editMenu->addSeparator();
  editMenu->addAction(selectAct);

  analysisMenu = menuBar()->addMenu(tr("&Analysis"));
  analysisMenu->addSeparator()->setText(tr("Statistics"));
  analysisMenu->addAction(statisAct);
  analysisMenu->addSeparator()->setText(tr("Structure"));
  analysisMenu->addAction(rdfShowAct);
  analysisMenu->addAction(rdfCalcAct);
  analysisMenu->addAction(skAct);
  analysisMenu->addAction(zDensityAct);
  analysisMenu->addSeparator()->setText(tr("Dynamics"));
  analysisMenu->addAction(msdAct);
  analysisMenu->addAction(vafAct);
  analysisMenu->addAction(fafAct);

  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct);
}

void DLWizard::closeEvent(QCloseEvent *) { emit closing(); }

DLWizard::~DLWizard() {}
