//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include <QString>

#include "DLPlotter.h"

void DLPlotter::moveLegend() {
  if (QAction *contextAction = qobject_cast<QAction *>(sender())) {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok) {
      plot->axisRect()->insetLayout()->setInsetAlignment(
         0, (Qt::Alignment)dataInt);
      plot->replot();
    }
  }
}

void DLPlotter::removeAllGraphs() {
  plot->clearGraphs();
  plot->replot();
}

void DLPlotter::contextMenuRequest(QPoint pos) {
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);

  if (plot->legend->selectTest(pos, false) >= 0) {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))
       ->setData((int)(Qt::AlignTop | Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))
       ->setData((int)(Qt::AlignTop | Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))
       ->setData((int)(Qt::AlignTop | Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))
       ->setData((int)(Qt::AlignBottom | Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))
       ->setData((int)(Qt::AlignBottom | Qt::AlignLeft));
  } else {
    if (plot->selectedGraphs().size() > 0)
      menu->addAction("Remove selected graph", this,
                      SLOT(removeSelectedGraph()));
    if (plot->graphCount() > 0)
      menu->addAction("Remove all graphs", this, SLOT(removeAllGraphs()));
  }

  menu->popup(plot->mapToGlobal(pos));
}

void DLPlotter::axisLabelDoubleClick(QCPAxis *axis,
                                     QCPAxis::SelectablePart part) {
  if (part == QCPAxis::spAxisLabel) {
    bool ok;
    QString newLabel =
       QInputDialog::getText(this, "QCustomPlot example", "New axis label:",
                             QLineEdit::Normal, axis->label(), &ok);
    if (ok) {
      axis->setLabel(newLabel);
      plot->replot();
    }
  }
}

void DLPlotter::legendDoubleClick(QCPLegend *legend,
                                  QCPAbstractLegendItem *item) {}
void DLPlotter::titleDoubleClick(QMouseEvent *event) {
  Q_UNUSED(event);
    if (QCPTextElement *title = qobject_cast<QCPTextElement*>(sender())){              
      // Set the plot title by double clicking on it
      bool ok;     
      QString newTitle = QInputDialog::getText(this, "DL_POLY_4 Statistics", "New plot title:", QLineEdit::Normal, title->text(), &ok);
      if (ok) {            
        title->setText(newTitle);
        plot->replot();
      }            
    }
}

void DLPlotter::removeSelectedGraph() {
  if (plot->selectedGraphs().size() > 0) {
    plot->removeGraph(plot->selectedGraphs().first());
    plot->replot();
  }
}

void DLPlotter::mouseWheel() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeZoom(plot->xAxis->orientation());
  else if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeZoom(plot->yAxis->orientation());
  else
    plot->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical);
}

void DLPlotter::mousePress() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeDrag(plot->xAxis->orientation());
  else if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeDrag(plot->yAxis->orientation());
  else
    plot->axisRect()->setRangeDrag(Qt::Horizontal | Qt::Vertical);
}

void DLPlotter::selectionChanged() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      plot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    plot->xAxis2->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
    plot->xAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
  }
  if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      plot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    plot->yAxis2->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
    plot->yAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
  }

  for (auto i = 0; i < plot->graphCount(); ++i) {
    QCPGraph *graph = plot->graph(i);
    QCPPlottableLegendItem *item = plot->legend->itemWithPlottable(graph);
    if (item->selected() || graph->selected()) {
      item->setSelected(true);
      graph->setSelection(QCPDataSelection(graph->data()->dataRange()));
    }
  }
}
