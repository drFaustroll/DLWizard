//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef DLWIZARD_H
#define DLWIZARD_H

#include <QMainWindow>
#include "DLStatis.h"
#include "DLFrame.h"
#include "DLStatisPlotter.h"

QT_BEGIN_NAMESPACE
class QAction;
class QTextEdit;
class QMenu;
QT_END_NAMESPACE

class DLWizard : public QMainWindow {
  Q_OBJECT

public:
  DLWizard(QMainWindow *parent = nullptr);
  ~DLWizard();

private slots:
  void loadStatis();
  void loadFrame();
  void loadTrajectory();
  void save();
  void print();

  void copy();
  void clear();
  void select();

  void statis();
  void rdfShow();
  void rdfCalc();
  void sk();
  void zDensity();
  void msd();
  void vaf();
  void faf();

  void about();
  void aboutQt();

signals:
  void closing();

protected:
  void closeEvent(QCloseEvent *);

private:
  void createActions();
  void createMenus();

  QMenu *fileMenu;
  QMenu *editMenu;
  QMenu *analysisMenu;
  QMenu *helpMenu;

  QAction *loadStatisAct;
  QAction *loadFrameAct;
  QAction *loadTrajAct;
  QAction *saveAct;
  QAction *printAct;
  QAction *exitAct;

  QAction *copyAct;
  QAction *clearAct;
  QAction *selectAct;

  QAction *statisAct;
  QAction *rdfShowAct;
  QAction *rdfCalcAct;
  QAction *skAct;
  QAction *zDensityAct;
  QAction *msdAct;
  QAction *vafAct;
  QAction *fafAct;

  QAction *aboutAct;
  QAction *aboutQtAct;

  QTextEdit *info = nullptr;
  QWidget *widget = nullptr;
  DLStatis *dlStatis = nullptr;
  DLFrame *dlFrame = nullptr;
};

#endif
