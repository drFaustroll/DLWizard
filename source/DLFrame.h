//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef DLFrame_H
#define DLFrame_H
#include <string>
#include <QString>
#include <QTextEdit>
#include <vector>
#include <set>
#include "atom.h"

class DLFrame {
public:
  DLFrame(const QString &frame, QTextEdit *log);
  ~DLFrame();
  int getNAtoms() const;
  int getNSpecies() const;

private:
  QString configLevel(int);
  QString cellLevel(int);

  QString frameFile;
  QTextEdit *log;
  QString frameTitle;
  bool hasVelocity = false;
  bool hasForce = false;
  int cType = 0;
  int cellType;
  double cell[3][3];
  int readBlock = 2;
  double *blockData = nullptr;
  std::set<QString> elements;
  std::vector<atom> atoms;
  int nAtoms;
  int nSpecies;
};
#endif
