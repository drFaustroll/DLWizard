//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include "DLFrame.h"
#include <QFile>
#include <QTextStream>

using namespace std;

DLFrame::DLFrame(const QString &frame, QTextEdit *log)
  : frameFile(frame), log(log) {
  QFile sFrame(frameFile);
  if (sFrame.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QTextStream in(&sFrame);
    QString line;
    QStringList all;
    int k = 0;
    int rc = 0;
    string element;
    int label;
    while (!in.atEnd()) {
      line = in.readLine();
      if (k == 0) {
        log->append(line);
        frameTitle = line;
      } else if (k == 1) {
        all = line.split(" ", QString::SkipEmptyParts);
        if (all.size() < 2) {
          log->append("<b>Error</b> reading line two from header!!!");
          break;
        } else {
          log->append(configLevel(all[0].toInt()));
          if (cType > 2)
            break;
          log->append(cellLevel(all[1].toInt()));
          blockData = new double[(readBlock - 1) * 3];
          if (cellType == 42)
            break;
        }
      } else {
        if (cellType > 0) {
          all = line.split(" ", QString::SkipEmptyParts);
          if (rc < 3) {
            for (int i = 0; i < 3; ++i)
              cell[rc][i] = all[i].toDouble();
            log->append(QString("%1 ").arg(cell[rc][0], 24, 'E', 16) +
                        QString("%1 ").arg(cell[rc][1], 24, 'E', 16) +
                        QString("%1").arg(cell[rc][2], 24, 'E', 16));
            rc++;
            k++;
            continue;
          }
        }
        auto cline = k - rc - 1;
        auto cr = cline % readBlock;
        all = line.split(" ", QString::SkipEmptyParts);
        if (cr == 1) {
          elements.insert(all[0]);
          element = all[0].toLocal8Bit().constData();
          label = all[1].toInt();
        } else {
          auto i = (readBlock - 2 - (cr ? (readBlock - cr) : 0)) * 3;
          blockData[i] = all[0].toDouble();
          blockData[i + 1] = all[1].toDouble();
          blockData[i + 2] = all[2].toDouble();
        }
        if (cr == 0) {
          if (readBlock == 2)
            atoms.push_back(atom(blockData[0], blockData[1], blockData[2], 0.0,
                                 0.0, 0.0, 0.0, 0.0, 0.0, label = label,
                                 element = element));
          if (readBlock == 3)
            atoms.push_back(atom(blockData[0], blockData[1], blockData[2],
                                 blockData[3], blockData[4], blockData[5], 0.0,
                                 0.0, 0.0, label = label, element = element));
          if (readBlock == 4)
            atoms.push_back(atom(blockData[0], blockData[1], blockData[2],
                                 blockData[3], blockData[4], blockData[5],
                                 blockData[6], blockData[7], blockData[8],
                                 label = label, element = element));
        }
      }
      k++;
    }
    sFrame.close();
    nAtoms = atoms.size();
    nSpecies = elements.size();
  } else {
    log->append("cannot open file " + frameFile);
  }
}

QString DLFrame::configLevel(int level) {
  QString ans;
  switch (level) {
  case 0:
    cType = level;
    readBlock = 2;
    ans = QString("coordinates only included in file");
    break;
  case 1:
    cType = level;
    readBlock = 3;
    ans = QString("coordinates and velocities included in file");
    break;
  case 2:
    cType = level;
    readBlock = 4;
    ans = QString("coordinates, velocities and forces included in file");
    break;
  default:
    cType = level;
    ans = QString("unknown config level specified");
  }
  return ans;
}

QString DLFrame::cellLevel(int level) {
  QString ans;
  switch (level) {
  case 0:
    cellType = level;
    ans = QString("no periodic boundary conditions");
    break;
  case 1:
    cellType = level;
    ans = QString("cubic boundary conditions");
    break;
  case 2:
    cellType = level;
    ans = QString("orthorombic boundary conditions");
    break;
  case 3:
    cellType = level;
    ans = QString("parallelepiped boundary conditions");
    break;
  case 6:
    cellType = level;
    ans = QString(
       "x-y parallelogram boundary conditions with no periodicity in z");
    break;
  default:
    cellType = 42;
    ans = QString("unknown periodicity specified");
  }
  return ans;
}

int DLFrame::getNAtoms() const { return nAtoms; }

int DLFrame::getNSpecies() const { return nSpecies; }

DLFrame::~DLFrame() {
  if (blockData != nullptr)
    delete[] blockData;
}
