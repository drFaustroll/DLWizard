//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef DLSTATIS_H
#define DLSTATIS_H

#include <QTextEdit>
#include <QString>
#include <algorithm>

class DLStatis {
public:
  DLStatis(const QString &fileName, QTextEdit *log);
  ~DLStatis(){};
  int getLength() { return datumLists[0].size(); };
  QVector<double> *getTime() { return &time; };
  QVector<double> *getEnergy() { return &datumLists[0]; };
  QVector<double> *getAY(int i = 0) { return &datumLists[i]; };
  double xmin() { return *std::min_element(time.begin(), time.end()); };
  double xmax() { return *std::max_element(time.begin(), time.end()); };
  double ymin(int i = 0) {
    return *std::min_element(datumLists[i].begin(), datumLists[i].end());
  };
  double ymax(int i = 0) {
    return *std::max_element(datumLists[i].begin(), datumLists[i].end());
  };
  void setNames();
  QString getName(int i = 0) { return datumNames[i]; };
  int getDatumN() { return numDatum; };

private:
  QString statTitle;
  QString units;
  QString statFile;
  QTextEdit *log;
  QVector<int> steps;
  QVector<double> time;
  int numDatum = 0;
  QStringList datumNames;
  std::vector<QVector<double>> datumLists;
};
#endif
