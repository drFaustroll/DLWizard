//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include "DLStatis.h"
#include <QFile>
#include <QTextStream>
#include <iostream>

DLStatis::DLStatis(const QString &fileName, QTextEdit *log)
  : statFile(fileName), log(log) {
  QFile sStatis(statFile);
  if (sStatis.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QTextStream in(&sStatis);
    QString line;
    QStringList all;

    int k = 1;
    int j = 10000000;
    bool readN = true;
    while (!in.atEnd()) {
      line = in.readLine();
      if (k == 1) {
        statTitle = line;
        k++;
        log->append(statTitle);
        continue;
      }
      if (k == 2) {
        units = line;
        k++;
        log->append(units);
        continue;
      }
      all = line.split(" ", QString::SkipEmptyParts);
      if ((all.size() == 3)&& (j>numDatum/5)) {
        time.push_back(all[1].toDouble());
        steps.push_back(all[0].toInt());
        if (numDatum == 0) {
          numDatum = all[2].toInt();
          log->append(
             QString("reading %1 data series from file").arg(numDatum));
          datumLists.resize(numDatum);
        }
        j = 0;
        continue;

      }
      for (auto l = 0; l < 5; ++l){
        if (5*j+l<numDatum) {
          datumLists[5 * j + l].push_back(all[l].toDouble());
        }
      }
      j++;
      
    }
    sStatis.close();
    setNames();
  } else {
    log->append("cannot open file " + statFile);
  }
}
void DLStatis::setNames() {
  datumNames << "1-1 total extended system energy "
             << "1-2 system temperature "
             << "1-3 configurational energy "
             << "1-4 short range potential energy "
             << "1-5 electrostatic energy "
             << "2-1 chemical bond energy "
             << "2-2 valence angle and 3-body potential energy "
             << "2-3 dihedral, inversion, and 4-body potential energy "
             << "2-4 tethering energy "
             << "2-5 enthalpy (total energy + PV) "
             << "3-1 rotational temperature "
             << "3-2 total virial "
             << "3-3 short-range virial "
             << "3-4 electrostatic virial "
             << "3-5 bond virial "
             << "4-1 valence angle and 3-body virial "
             << "4-2 constraint bond virial "
             << "4-3 tethering virial "
             << "4-4 volume "
             << "4-5 core-shell temperature "
             << "5-1 core-shell potential energy "
             << "5-2 core-shell virial "
             << "5-3 MD cell angle α "
             << "5-4 MD cell angle β "
             << "5-5 MD cell angle γ "
             << "6-1 PMF constraint virial "
             << "6-2 pressure "
             << "6-3 exdof ";

  for (auto i = 28; i < numDatum; ++i)
    datumNames << QString("%1-%2 %3").arg(QString("%1").arg(i/5+1),
        QString("%1").arg(i%5+1),QString("%1").arg(i+1));
}
