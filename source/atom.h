//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef ATOM_H
#define ATOM_H
#include <string>

class atom {
public:
  atom(double x, double y, double z, double vx = 0, double vy = 0,
       double vz = 0, double fx = 0, double fy = 0, double fz = 0,
       int label = 1, std::string element = "X");
  ~atom(){};

private:
  double x, y, z;
  double vx, vy, vz;
  double fx, fy, fz;
  int label;
  std::string element;
};
#endif
