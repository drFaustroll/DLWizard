//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include <QString>
#include <iostream>

#include "DLStatisPlotter.h"
#include "utils.h"
DLStatisPlotter::DLStatisPlotter(DLStatis *dlStatis, QTextEdit *log,
    QWidget *parent)
  : dlStatis(dlStatis), DLPlotter(parent), log(log) {
    wp = new QWidget();
    wp->setAttribute(Qt::WA_DeleteOnClose);
    QGridLayout *wpLayout = new QGridLayout(wp);
    plot = new QCustomPlot(wp);
    QRect screen = QDesktopWidget().screenGeometry(0);

    connect(plot, SIGNAL(mouseWheel(QWheelEvent *)), this, SLOT(mouseWheel()));
    connect(plot, SIGNAL(mousePress(QMouseEvent *)), this, SLOT(mousePress()));
    connect(plot, SIGNAL(mouseDoubleClick(QMouseEvent*)), this,
        SLOT(titleDoubleClick(QMouseEvent *)));
    connect(plot, SIGNAL(axisDoubleClick(QCPAxis *, QCPAxis::SelectablePart,
            QMouseEvent *)),
        this, SLOT(axisLabelDoubleClick(QCPAxis *, QCPAxis::SelectablePart)));
//    connect(plot, SIGNAL(legendDoubleClick(QCPLegend *, QCPAbstractLegendItem *,QMouseEvent *)),
//        this, SLOT(legendDoubleClick(QCPLegend *, QCPAbstractLegendItem *)));
    connect(plot, SIGNAL(selectionChangedByUser()), this,
        SLOT(selectionChanged()));
    connect(plot, SIGNAL(customContextMenuRequested(QPoint)), this,
        SLOT(contextMenuRequest(QPoint)));
    connect(parent, SIGNAL(closing()), wp, SLOT(close()));

    plot->plotLayout()->insertRow(0);
    plot->plotLayout()->addElement(
        0, 0, new QCPTextElement(plot, "DL_POLY_4 STATIS", QFont("sans", 17, QFont::Bold)));
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
        QCP::iSelectLegend | QCP::iSelectPlottables);

    plot->legend->setVisible(true);
    plot->axisRect()->setupFullAxesBox();
    plot->legend->setSelectableParts(QCPLegend::spItems);
    plot->setContextMenuPolicy(Qt::CustomContextMenu);

    addGraph(0);
    wpLayout->addWidget(plot, 0, 0, 10, 5);

    graphsCombo = new QComboBox;
    gLabel = new QLabel("Add Graph: ");
    for (int i = 0; i < dlStatis->getDatumN(); ++i)
      graphsCombo->addItem(dlStatis->getName(i), i);
    connect(graphsCombo, SIGNAL(currentIndexChanged(int)), this,
        SLOT(addGraph(int)));

    wpLayout->addWidget(gLabel, 11, 0);
    wpLayout->addWidget(graphsCombo, 11, 1);

    createAnalysisGroup();
    wpLayout->addWidget(analysisGroup, 12, 0, 2, 1);
    connect(analysisButtons, SIGNAL(buttonClicked(int)), this,
        SLOT(analysisSelected(int)));
    createVarianceGroup();
    wpLayout->addWidget(varianceGroup, 14, 0, 2, 1);
    connect(varianceButtons, SIGNAL(buttonClicked(int)), this,
        SLOT(varianceSelected(int)));
    createHistoOptions();
    wpLayout->addWidget(histoGroup, 12, 1, 1, 1);
    connect(bins, SIGNAL(textChanged(const QString &)), this,
        SLOT(histoUpdate(QString)));
    connect(hNorm, SIGNAL(stateChanged(int)), this, SLOT(histoUpdate(int)));

    createAutoOptions();
    wpLayout->addWidget(lagGroup, 13, 1, 1, 1);
    connect(lags, SIGNAL(textChanged(const QString &)), this,
        SLOT(lagUpdate(QString)));

    createFTOptions();
    wpLayout->addWidget(ftGroup, 14, 1, 1, 1);

    createEnsembleOptions();
    wpLayout->addWidget(ensembleGroup, 15, 1, 1, 1);
    connect(ensembleStart, SIGNAL(textChanged(const QString &)), this,
        SLOT(aveUpdate(QString)));
    connect(ensembleEnd, SIGNAL(textChanged(const QString &)), this,
        SLOT(aveUpdate(QString)));
    connect(ensembleFreq, SIGNAL(textChanged(const QString &)), this,
        SLOT(aveUpdate(QString)));
    wp->setLayout(wpLayout);

    wp->setMinimumSize(screen.width() * 0.25, screen.height() * 0.5);
    wp->resize(screen.width() * 0.5, screen.height() * 1.0);
    wp->show();
  }

void DLStatisPlotter::createAnalysisGroup() {
  analysisGroup = new QGroupBox(tr("Select type of Graph"), this);
  analysisButtons = new QButtonGroup(this);
  rTL = new QRadioButton(tr("&Time Line"), this);
  rHist = new QRadioButton(tr("&Histogram"), this);
  rAuto = new QRadioButton(tr("&Autocorrelation"), this);
  rFT = new QRadioButton(tr("&Fourier Transform"), this);
  ensembleAve = new QRadioButton(tr("&Running Average"), this);

  analysisButtons->addButton(rTL, 0);
  analysisButtons->addButton(rHist, 1);
  analysisButtons->addButton(rAuto, 2);
  analysisButtons->addButton(rFT, 3);
  analysisButtons->addButton(ensembleAve, 4);
  rTL->setChecked(true);

  analysisLayout = new QVBoxLayout;
  analysisLayout->addWidget(rTL);
  analysisLayout->addWidget(rHist);
  analysisLayout->addWidget(rAuto);
  analysisLayout->addWidget(rFT);
  analysisLayout->addWidget(ensembleAve);
  analysisLayout->addStretch(1);
  analysisGroup->setLayout(analysisLayout);
}

void DLStatisPlotter::createVarianceGroup() {
  varianceGroup = new QGroupBox(tr("Variance"), this);
  varianceButtons = new QButtonGroup(this);
  rNormal = new QRadioButton(tr("&Normal"), this);
  rWindow = new QRadioButton(tr("&Window"), this);
  rJackKnife = new QRadioButton(tr("&Jack Knife"), this);
  varianceButtons->addButton(rNormal, 0);
  varianceButtons->addButton(rWindow, 1);
  varianceButtons->addButton(rJackKnife, 2);
  rNormal->setChecked(true);
  varianceLayout = new QVBoxLayout;
  varianceLayout->addWidget(rNormal);
  varianceLayout->addWidget(rWindow);
  varianceLayout->addWidget(rJackKnife);
  varianceLayout->addStretch(1);
  varianceGroup->setLayout(varianceLayout);
  varianceGroup->setEnabled(false);
}
void DLStatisPlotter::createHistoOptions() {
  histoGroup = new QGroupBox(tr("Histogram Options"), this);
  hLabel = new QLabel(tr("Bins: "), this);
  bins = new QLineEdit(this);
  hnLabel = new QLabel(tr("Normalize:"), this);
  hNorm = new QCheckBox(this);
  bins->setValidator(new QIntValidator(0, 1000000, this));
  bins->setText("100");
  histoLayout = new QHBoxLayout(this);
  histoLayout->addWidget(hLabel);
  histoLayout->addWidget(bins);
  histoLayout->addWidget(hnLabel);
  histoLayout->addWidget(hNorm);
  histoGroup->setLayout(histoLayout);
  histoGroup->setEnabled(false);
}

void DLStatisPlotter::createAutoOptions() {
  lagGroup = new QGroupBox(tr("AutoCorrelation Options"), this);
  lLabel = new QLabel(tr("Lag: "), this);
  lags = new QLineEdit(this);
  lags->setValidator(new QIntValidator(0, 1000, this));
  lags->setText("250");
  lagLayout = new QHBoxLayout(this);
  lagLayout->addWidget(lLabel);
  lagLayout->addWidget(lags);
  lagGroup->setLayout(lagLayout);
  lagGroup->setEnabled(false);
}
void DLStatisPlotter::createFTOptions() {
  ftGroup = new QGroupBox(tr("Fourier Transform Options"), this);
  ftLayout = new QHBoxLayout(this);
  ftLabel = new QLabel(tr("FT: "), this);
  ftLayout->addWidget(ftLabel);
  ftGroup->setLayout(ftLayout);
  ftGroup->setEnabled(false);
}

void DLStatisPlotter::createEnsembleOptions() {
  ensembleGroup = new QGroupBox(tr("Running Average Options"), this);
  ensembleLayout = new QHBoxLayout(this);
  ensembleSLabel = new QLabel(tr("Start at: "), this);
  ensembleELabel = new QLabel(tr("End at: "), this);
  ensembleStart = new QLineEdit(this);
  ensembleStart->setValidator(new QDoubleValidator(0, 10000000,4, this));
  ensembleStart->setText("0.0");
  ensembleEnd = new QLineEdit(this);
  ensembleEnd->setValidator(new QDoubleValidator(0, 10000000,4, this));
  ensembleEnd->setText("0.0");
  ensembleFLabel = new QLabel(tr("Frequency: "), this);
  ensembleFreq = new QLineEdit(this);
  ensembleFreq->setValidator(new QIntValidator(1, 10000000, this));
  ensembleFreq->setText("250");
  ensembleLayout->addWidget(ensembleSLabel);
  ensembleLayout->addWidget(ensembleStart);
  ensembleLayout->addWidget(ensembleELabel);
  ensembleLayout->addWidget(ensembleEnd);
  ensembleLayout->addWidget(ensembleFLabel);
  ensembleLayout->addWidget(ensembleFreq);
  ensembleGroup->setLayout(ensembleLayout);
  ensembleGroup->setEnabled(false);
}

void DLStatisPlotter::analysisSelected(int i) {

  switch (i) {
    case TIMELINE:
      histoGroup->setEnabled(false);
      lagGroup->setEnabled(false);
      varianceGroup->setEnabled(false);
      ensembleGroup->setEnabled(false);
      varianceGroup->setEnabled(false);
      analysis = 0;
      removeAllGraphs();
      addGraph(graphsCombo->currentIndex());
      break;
    case HISTOGRAM:
      histoGroup->setEnabled(true);
      lagGroup->setEnabled(false);
      ftGroup->setEnabled(false);
      ensembleGroup->setEnabled(false);
      varianceGroup->setEnabled(false);
      analysis = 1;
      w = bins->text().toInt();
      normalize = hNorm->checkState();
      removeAllGraphs();
      addGraph(graphsCombo->currentIndex());
      break;
    case AUTO:
      histoGroup->setEnabled(false);
      lagGroup->setEnabled(true);
      ftGroup->setEnabled(false);
      ensembleGroup->setEnabled(false);
      varianceGroup->setEnabled(false);
      analysis = 2;
      w = lags->text().toInt();
      removeAllGraphs();
      addGraph(graphsCombo->currentIndex());
      break;
    case FFT:
      analysis = 3;
      histoGroup->setEnabled(false);
      lagGroup->setEnabled(false);
      ftGroup->setEnabled(true);
      ensembleGroup->setEnabled(false);
      varianceGroup->setEnabled(false);
      removeAllGraphs();
      addGraph(graphsCombo->currentIndex());
      break;
    case AVERAGE:
      analysis = 4;
      histoGroup->setEnabled(false);
      lagGroup->setEnabled(false);
      ftGroup->setEnabled(false);
      varianceGroup->setEnabled(true);
      ensembleGroup->setEnabled(true);
      start = ensembleStart->text().toDouble();
      end = ensembleEnd->text().toDouble();
      freq = ensembleFreq->text().toInt();
      removeAllGraphs();
      addGraph(graphsCombo->currentIndex());
      break;
  }
}

void DLStatisPlotter::moveLegend() {
  if (QAction *contextAction = qobject_cast<QAction *>(sender())) {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok) {
      plot->axisRect()->insetLayout()->setInsetAlignment(
          0, (Qt::Alignment)dataInt);
      plot->replot();
    }
  }
}

void DLStatisPlotter::addGraph(int i) {
  double ym, yM;
  QString y;
  QVector<double> xd;
  QVector<double> yd;
  QPen graphPen;
  int a, b, c;
  a = rand() % 245 + 10;
  b = rand() % 245 + 10;
  c = rand() % 245 + 10;
  int wpen = 1;
  switch (analysis) {
    case TIMELINE:
      ym = dlStatis->ymin(i);
      yM = dlStatis->ymax(i);
      if (std::abs(ym - yM) < 1.0e-12) {
        ym--;
        yM++;
      }
      plot->yAxis->setRange(ym, yM);
      plot->xAxis->setRange(dlStatis->xmin(), dlStatis->xmax());
      plot->addGraph();
      y = dlStatis->getName(i);
      plot->yAxis->setLabel(y);
      plot->xAxis->setLabel("time (ps)");
      plot->graph()->setName(y);
      plot->graph()->setData(*dlStatis->getTime(), *dlStatis->getAY(i));
      utils::dumpToFile(y.trimmed(),*dlStatis->getTime(), *dlStatis->getAY(i));
      break;
    case HISTOGRAM:
      ym = dlStatis->ymin(i);
      yM = dlStatis->ymax(i);
      if (std::abs(ym - yM) < 1.0e-12) {
        plot->xAxis->setRange(ym--, yM++);
      } else {
        plot->xAxis->setRange(ym, yM);
      }
      plot->addGraph();
      y = dlStatis->getName(i);
      plot->graph()->setName(y);
      plot->xAxis->setLabel(y);
      plot->yAxis->setLabel("Count");
      if (normalize == Qt::Unchecked) {
        utils::histogram(*dlStatis->getAY(i), w, ym, yM, yd, xd);
      } else {
        utils::histogramNormalized(*dlStatis->getAY(i), w, ym, yM, yd, xd);
      }
      plot->yAxis->setRange(0, *std::max_element(yd.begin(), yd.end()));
      plot->graph()->setData(xd, yd);
      break;
    case AUTO:
      plot->xAxis->setRange(1, w);
      plot->yAxis->setRange(-1, 1);
      plot->addGraph();
      y = dlStatis->getName(i);
      plot->graph()->setName(y);
      plot->xAxis->setLabel("Lags");
      plot->yAxis->setLabel("Autocorrelation");
      utils::autocorrelation(*dlStatis->getAY(i), w, xd, yd);
      plot->graph()->setData(xd, yd);
      break;
    case FFT:
      plot->xAxis->setRange(1, w);
      plot->yAxis->setRange(-1, 1);
      plot->addGraph();
      y = dlStatis->getName(i);
      plot->graph()->setName(y);
      plot->xAxis->setLabel("ω");
      plot->yAxis->setLabel("FFT");
      break;
    case AVERAGE:
      utils::runningAverage(*dlStatis->getTime(), *dlStatis->getAY(i), start, end,
          freq, xd, yd);
      ym = dlStatis->ymin(i);
      yM = dlStatis->ymax(i);
      if (std::abs(ym - yM) < 1.0e-12) {
        ym--;
        yM++;
      }
      plot->yAxis->setRange(ym, yM);
      plot->xAxis->setRange(dlStatis->xmin(), dlStatis->xmax());
      plot->addGraph();
      y = dlStatis->getName(i);
      plot->yAxis->setLabel(y);
      plot->xAxis->setLabel("time (ps)");
      plot->graph()->setName(y);
      log->append("Average " + y +
          QString(": %1, start=").arg(yd[yd.size()-1], 24, 'E', 16) +
          QString("%1, end=").arg(xd[0], 24, 'E', 16) +
          QString("%1,frequency=").arg(xd[xd.size()-1], 24, 'E', 16)+
          QString("%1,variance=").arg(freq) +
          QString("%1 not implemented").arg(0));
      plot->graph()->setData(*dlStatis->getTime(), *dlStatis->getAY(i));
      graphPen.setWidthF(0.25);
      graphPen.setColor(QColor(0.5 * a, 0.5 * b, 0.5 * c));
      plot->graph()->setPen(graphPen);
      plot->addGraph();
      plot->graph()->setName("Running Average " + y);
      plot->graph()->setData(xd, yd);
      wpen = 3;
      break;
  }
  plot->graph()->setLineStyle((QCPGraph::LineStyle)(1));
  graphPen.setColor(QColor(a, b, c));
  graphPen.setWidthF(wpen);
  plot->graph()->setPen(graphPen);
  plot->replot();
}

void DLStatisPlotter::removeAllGraphs() {
  plot->clearGraphs();
  plot->replot();
}

void DLStatisPlotter::contextMenuRequest(QPoint pos) {
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);

  if (plot->legend->selectTest(pos, false) >= 0) {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))
      ->setData((int)(Qt::AlignTop | Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))
      ->setData((int)(Qt::AlignTop | Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))
      ->setData((int)(Qt::AlignTop | Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))
      ->setData((int)(Qt::AlignBottom | Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))
      ->setData((int)(Qt::AlignBottom | Qt::AlignLeft));
  } else {
    if (plot->selectedGraphs().size() > 0)
      menu->addAction("Remove selected graph", this,
          SLOT(removeSelectedGraph()));
    if (plot->graphCount() > 0)
      menu->addAction("Remove all graphs", this, SLOT(removeAllGraphs()));
  }

  menu->popup(plot->mapToGlobal(pos));
}

void DLStatisPlotter::axisLabelDoubleClick(QCPAxis *axis,
    QCPAxis::SelectablePart part) {
  if (part == QCPAxis::spAxisLabel) {
    bool ok;
    QString newLabel =
      QInputDialog::getText(this, "QCustomPlot example", "New axis label:",
          QLineEdit::Normal, axis->label(), &ok);
    if (ok) {
      axis->setLabel(newLabel);
      plot->replot();
    }
  }
}

void DLStatisPlotter::legendDoubleClick(QCPLegend *legend,
    QCPAbstractLegendItem *item) {}
void DLStatisPlotter::titleDoubleClick(QMouseEvent *event) {
  Q_UNUSED(event);
    if (QCPTextElement *title = qobject_cast<QCPTextElement*>(sender())){              
      // Set the plot title by double clicking on it
      bool ok;     
      QString newTitle = QInputDialog::getText(this, "DL_POLY_4 Statistics", "New plot title:", QLineEdit::Normal, title->text(), &ok);
      if (ok) {            
        title->setText(newTitle);
        plot->replot();
      }            
    }
}

void DLStatisPlotter::removeSelectedGraph() {
  if (plot->selectedGraphs().size() > 0) {
    plot->removeGraph(plot->selectedGraphs().first());
    plot->replot();
  }
}

void DLStatisPlotter::mouseWheel() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeZoom(plot->xAxis->orientation());
  else if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeZoom(plot->yAxis->orientation());
  else
    plot->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical);
}

void DLStatisPlotter::mousePress() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeDrag(plot->xAxis->orientation());
  else if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    plot->axisRect()->setRangeDrag(plot->yAxis->orientation());
  else
    plot->axisRect()->setRangeDrag(Qt::Horizontal | Qt::Vertical);
}

void DLStatisPlotter::selectionChanged() {
  if (plot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      plot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    plot->xAxis2->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
    plot->xAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
  }
  if (plot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      plot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) ||
      plot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    plot->yAxis2->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
    plot->yAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels);
  }

  for (auto i = 0; i < plot->graphCount(); ++i) {
    QCPGraph *graph = plot->graph(i);
    QCPPlottableLegendItem *item = plot->legend->itemWithPlottable(graph);
    if (item->selected() || graph->selected()) {
      item->setSelected(true);
      graph->setSelection(QCPDataSelection(graph->data()->dataRange()));
    }
  }
}
