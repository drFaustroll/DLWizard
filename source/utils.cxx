//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include <algorithm>

#include "utils.h"
#include <cmath>
#include <iostream>
#include <fstream>

void utils::histogram(const QVector<double> &a, const int nBins,
                      const double &min, const double &max,
                      QVector<double> &hist, QVector<double> &bins) {
  if (hist.size() != 0) {
    hist.erase(hist.begin(), hist.end());
  }
  if (bins.size() != 0) {
    bins.erase(bins.begin(), bins.end());
  }
  double h = fabs(max - min) / nBins;
  for (int i = 0; i <= nBins; i++) {
    hist.push_back(0);
    bins.push_back(min + i * h);
  }
  int m;
  for (int i = 0; i < a.size(); ++i) {
    m = (int)((a[i] - min) / h);
    hist[m]++;
  }
}
void utils::histogram(const QVector<double> &a, const int nBins,
                      QVector<double> &hist, QVector<double> &bins) {
  double max = *std::max_element(a.begin(), a.end());
  double min = *std::min_element(a.begin(), a.end());
  histogram(a, nBins, min, max, hist, bins);
}
void utils::histogramNormalized(const QVector<double> &a, const int nBins,
                                const double &min, const double &max,
                                QVector<double> &hist, QVector<double> &bins) {
  histogram(a, nBins, min, max, hist, bins);
  double s = 0.0;
  double bin = bins[1] - bins[0];
  for (int i = 0; i < hist.size(); i++) {
    s += bin * hist[i];
  }
  for (int i = 0; i < hist.size(); i++) {
    hist[i] = hist[i] / s;
  }
}
void utils::autocorrelation(const QVector<double> &a, const int nLags,
                            QVector<double> &x, QVector<double> &c) {
  if (x.size() != 0) {
    x.erase(x.begin(), x.end());
  }
  if (c.size() != 0) {
    c.erase(c.begin(), c.end());
  }
  double av = 0.0;
  for (auto i = 0; i < a.size(); ++i) {
    av += a[i];
  }
  av = av / a.size();
  for (int h = 0; h <= nLags; ++h) {
    c.push_back(0.0);
    x.push_back(h);
    for (auto i = 0; i < a.size() - h; ++i) {
      c[h] += (a[i] - av) * (a[i + h] - av);
    }
    c[h] = c[h] / a.size();
  }
  for (int h = 1; h <= nLags; ++h)
    c[h] = c[h] / c[0];
}

void utils::runningAverage(const QVector<double> &x, const QVector<double> &y,
                           const float start, const float end, const int step, QVector<double> &xd,
                           QVector<double> &yd) {

  if (xd.size() != 0) {
    xd.erase(xd.begin(), xd.end());
  }
  if (yd.size() != 0) {
    yd.erase(yd.begin(), yd.end());
  }
  int istart=0;
  int iend=x.size();
  for (auto i = 0; i < iend; ++i) {
    if (x[i]>= start){
      istart = i;
      break;
    }
    }
   for(auto i=istart;i<x.size();++i){
    if (x[i]>= end){
      iend = i+1;
      break;
    }
   
   }
   double av = 0.0;
  auto k = 1;
  for (auto i = istart; i < iend; i += step) {
    av += y[i];
    xd.push_back(x[i]);
    yd.push_back(av / static_cast<double>(k));
    k++;
  }
}

void utils::dumpToFile(const QString &filename, QVector<double> &x, QVector<double> &y){
  using namespace std;
  ofstream ts(filename.toStdString().c_str(),ios::out);

  if (ts.is_open()){
    ts.setf(std::ios_base::fixed, std::ios_base::floatfield);
    ts.width(10);
    ts.precision(6);
    //for(auto i=0;i<x.size();++i) ts<<setw(10)<<x[i]<<" "<<setw(12)<<y[i]<<endl;
    for(auto i=0;i<x.size();++i) {
      ts<<x[i]<<" "<<y[i]<<endl;
    }
    ts.close();
  }else{
    cout<<"coule not open file "<<filename.toStdString()<<endl;
  }
}
