//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef DLSTATISPLOTTER_H
#define DLSTATISPLOTTER_H
#include <QWidget>

#include "DLStatis.h"
#include "DLPlotter.h"
#ifdef IQCP
#include "extra/qcustomplot-source/qcustomplot.h"
#else
#include "qcustomplot.h"
#endif
class DLStatisPlotter : public DLPlotter {
  Q_OBJECT
public:
  DLStatisPlotter(DLStatis *dlStatis = nullptr, QTextEdit *log = nullptr,
                  QWidget *parent = nullptr);
  ~DLStatisPlotter(){};

private slots:
  void mousePress();
  void mouseWheel();

  void titleDoubleClick(QMouseEvent *event);
  void axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part);
  void legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item);
  void selectionChanged();
  void removeSelectedGraph();
  void removeAllGraphs();
  void contextMenuRequest(QPoint pos);
  void addGraph(int i = 0);
  void moveLegend();
  void analysisSelected(int);
  void varianceSelected(int){};
  void histoUpdate(QString dummy) { analysisSelected(HISTOGRAM); };
  void histoUpdate(int dummy) { analysisSelected(HISTOGRAM); };
  void lagUpdate(QString dummy) { analysisSelected(AUTO); };
  void aveUpdate(QString dummy) { analysisSelected(AVERAGE); };

private:
  void createAnalysisGroup();
  void createHistoOptions();
  void createAutoOptions();
  void createFTOptions();
  void createEnsembleOptions();
  void createVarianceGroup();
  enum Analysis { TIMELINE, HISTOGRAM, AUTO, FFT, AVERAGE };
  QTextEdit *log = nullptr;

  DLStatis *dlStatis = nullptr;
  QWidget *wp = nullptr;
  QCustomPlot *plot = nullptr;

  QComboBox *graphsCombo = nullptr;
  QLabel *gLabel = nullptr;

  QGroupBox *analysisGroup = nullptr;
  QRadioButton *rTL = nullptr;
  QRadioButton *rHist = nullptr;
  QRadioButton *rAuto = nullptr;
  QRadioButton *rFT = nullptr;
  QRadioButton *ensembleAve = nullptr;
  QVBoxLayout *analysisLayout = nullptr;
  QButtonGroup *analysisButtons = nullptr;

  // histogram
  QLineEdit *bins = nullptr;
  QGroupBox *histoGroup = nullptr;
  QHBoxLayout *histoLayout = nullptr;
  QLabel *hLabel = nullptr;
  QLabel *hnLabel = nullptr;
  QCheckBox *hNorm = nullptr;
  // autocorrelation
  QLineEdit *lags = nullptr;
  QGroupBox *lagGroup = nullptr;
  QHBoxLayout *lagLayout = nullptr;
  QLabel *lLabel = nullptr;
  // FT
  QGroupBox *ftGroup = nullptr;
  QHBoxLayout *ftLayout = nullptr;
  QLabel *ftLabel = nullptr;
  // Ensemble Running Average
  //
  QGroupBox *ensembleGroup = nullptr;
  QHBoxLayout *ensembleLayout = nullptr;
  QLabel *ensembleSLabel = nullptr;
  QLineEdit *ensembleStart = nullptr;
  QLabel *ensembleELabel = nullptr;
  QLineEdit *ensembleEnd = nullptr;
  QLabel *ensembleFLabel = nullptr;
  QLineEdit *ensembleFreq = nullptr;

  // variance options
  QGroupBox *varianceGroup = nullptr;
  QVBoxLayout *varianceLayout = nullptr;
  QButtonGroup *varianceButtons = nullptr;
  QRadioButton *rNormal = nullptr;
  QRadioButton *rWindow = nullptr;
  QRadioButton *rJackKnife = nullptr;

  int analysis = 0;
  int w = 0;
  int normalize = Qt::Unchecked;
  float start = 0.0f;
  float end = 0.0f;
  int freq = 1;
};
#endif
