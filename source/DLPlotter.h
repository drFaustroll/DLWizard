//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef DLPLOTTER_H
#define DLPLOTTER_H

#include <QWidget>
#include "DLStatis.h"
#ifdef IQCP
#include "extra/qcustomplot-source/qcustomplot.h"
#else
#include "qcustomplot.h"
#endif

class DLPlotter : public QWidget {
  Q_OBJECT
public:
  DLPlotter(QWidget *parent = nullptr) : QWidget(parent){};
  ~DLPlotter(){};

private slots:
  void mousePress();
  void mouseWheel();

  void titleDoubleClick(QMouseEvent *event);
  void axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part);
  void legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item);
  void selectionChanged();
  void removeSelectedGraph();
  void removeAllGraphs();
  void contextMenuRequest(QPoint pos);
  void addGraph(int i = 0){};
  void moveLegend();

private:
  QCustomPlot *plot = nullptr;
};
#endif
