//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#include <QApplication>
#include "DLWizard.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  app.setAttribute(Qt::AA_UseHighDpiPixmaps, true);
  DLWizard window;
  window.show();
  return app.exec();
}
