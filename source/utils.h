//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT
#ifndef UTILS_H
#define UTILS_H
#include <QVector>
#include <QString>
namespace utils {
void histogram(const QVector<double> &a, const int nBins, QVector<double> &hist,
               QVector<double> &bins);
void histogram(const QVector<double> &a, const int nBins, const double &min,
               const double &max, QVector<double> &hist, QVector<double> &bins);
void histogramNormalized(const QVector<double> &a, const int nBins,
                         const double &min, const double &max,
                         QVector<double> &hist, QVector<double> &bins);
void autocorrelation(const QVector<double> &a, const int nLags,
                     QVector<double> &x, QVector<double> &c);
void runningAverage(const QVector<double> &x, const QVector<double> &y,
                    const float start, const float end, const int step, QVector<double> &xd,
                    QVector<double> &yd);
void dumpToFile(const QString &filename,  QVector<double> &x, QVector<double> &y);
}
#endif
