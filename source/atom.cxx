//    Copyright (c) 2015 Alin Marin Elena <alin@elena.space>
//    The MIT License http://opensource.org/licenses/MIT

#include "atom.h"
atom::atom(double x, double y, double z, double vx, double vy, double vz,
           double fx, double fy, double fz, int label, std::string element)
  : x(x), y(y), z(z), vx(vx), vy(vy), vz(vz), fx(fx), fy(fy), fz(fz),
    label(label), element(element) {}
