DLWizard is an evening, weekend and bank holiday contribution 
to DL_POLY_4 project.
I shall allow one to visualize and explore different aspects of STATIS file
and other output files of DL_POLY_4, and maybe DL_POLY Classic.

Since this is a project funded by my time, will probably fit my needs, however 
if you find a bug or you have a wish, open an issue. There is no guarantee I will 
implement it but you never know sometimes is Christmas.

If you want to sponsor my time to work on a feature/bug of your choice please
feel free to contact me.
